package movies.tests;

import static org.junit.jupiter.api.Assertions.*;
import java.io.*;
import java.util.*;
import org.junit.jupiter.api.Test;


import movies.importer.*;;

/** 
 * Test the ImportPipeline for Exceptions
 * Catch IOException, NullPointerException, IndexOutOfBoundsException
 * @author Guang Zhang
 *
 */

class ImportPipelineTests {	
	
	@Test
	public void testIOException() {
		System.out.println("Test IOException");
	    try {
	    	Processor[] processes = new Processor[5];
	    	
	    	// Expected IOException due to wrong output directory
			String srcImdb="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//imdbTestFiles";
			String srcImported="C:\\cathy\\courses\\java310\\fall2020project01\\files\\standardizedTestFiles";
			ImdbImporter imdbImp = new ImdbImporter(srcImdb, srcImported);
			processes[0]=imdbImp;
			processes[0].execute();
	    } catch (IndexOutOfBoundsException e) {
	    	System.out.println("IndexOutofBoundsException: not being able to process due to wrong output directory");
	    } catch (NullPointerException e) {
	    	System.out.println("No such file found");
	    } catch (FileNotFoundException e) {
	    	System.out.println("No such file found");
	    } catch (IOException e) {
	    	System.out.println("IOException expected");
	    } catch(NumberFormatException e) {
				System.out.println("Wrong number format for ReleaseYear and/or RunTime");
		}
	}
	
	@Test
	public void testNullPointerError() {
		System.out.println("Test NullPointerError");
	    try {
	    	Processor[] processes = new Processor[5];
			
			// Expected NullPointerException due to wrong input directory
			String srcKaggle="C:\\cathy\\courses\\java310\\fall2020project01\\files\\kaggleTestFiles";
			String srcImported="C:\\cathy\\courses\\java310\\fall2020project01\\files\\standardizedTestFiles";
			KaggleImporter kaggleImp = new KaggleImporter(srcKaggle, srcImported);
			processes[1]=kaggleImp;
			processes[1].execute();
			
	    } catch (IndexOutOfBoundsException e) {
	    	System.out.println("IndexOutofBoundsException: not being able to process due to wrong output directory");
	    } catch (NullPointerException e) {
	    	System.out.println("NullPointerException: No such file found");
	    } catch (FileNotFoundException e) {
	    	System.out.println("No such file found");
	    } catch (IOException e) {
	    	System.out.println("IOException expected");
	    } catch(NumberFormatException e) {
				System.out.println("Wrong number format for ReleaseYear and/or RunTime");
		}
	}

}
