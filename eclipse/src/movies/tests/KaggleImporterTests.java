package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;

/**
 * JUnit Test for KaggleImporter
 * @author Cathy Tham
 *
 */
class KaggleImporterTests {
	
	/**
	 * Test the process method
	 */
	@Test
	void testProcess() {
		KaggleImporter test = new KaggleImporter(" "," ");
		ArrayList<String> testArrList = new ArrayList<String>();
		testArrList.add("Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		ArrayList<String> processTestArrList = test.process(testArrList);
		
		String expected="2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle";
		assertEquals(expected,processTestArrList.get(0));
		
	}
	
	/**
	 * Test the process method with the wrong column number
	 */
	@Test
	void testProcessLessColumns() {
		KaggleImporter test = new KaggleImporter(" "," ");
		ArrayList<String> testArrList = new ArrayList<String>();
		testArrList.add("Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		//remove a column from "The Mummy: Tomb of the Dragon Emperor"
		testArrList.add("John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		testArrList.add("Jack Nicholson	Warren Beatty	Stockard Channing	Florence Stanley	Richard B. Shull	Thomas Newman	\"Three's a crowd in Mike Nichols's period caper comedy -- or is it? To dodge the 1920s Mann Act barring the transport of women across state lines for \"\"immoral purposes,\"\" not-yet-divorced Nicky (Warren Beatty) has felonious buddy Oscar (Jack Nicholson) marry Nicky's runaway heiress sweetheart Freddy (Stockard Channing) so they can all escape New York for Los Angeles. The three set up house together, but trouble starts brewing when odd man out Oscar decides to get Nicky's attention by exercising his rights as a husband to Freddy. Exasperated with being stuck in the middle of the bickering pair, Freddy threatens to donate her impending inheritance to charity, inciting Oscar and Nicky to hatch a plan to bump her off and keep the money. But Freddy just will not die, prompting the three to reconsider the whole arrangement. With a period setting and pair of stellar lead actors similar to the 1973 blockbuster The Sting, a screenplay by Five Easy Pieces author Carol Eastman (under the name Adrien Joyce), and deft comedy director Nichols, The Fortune seemed like a can't-miss proposition. But it resoundingly flopped, as audiences preferred to see Beatty in his earlier 1975 starring role as a racy L.A. hairdresser in Shampoo, and to wait for Nicholson's later 1975 incarnation as an archetypal iconoclast in One Flew Over the Cuckoo's Nest. As with other late '60s-early '70s period films like Beatty's own Bonnie and Clyde (1967), The Fortune lends an updated sensibility to its old-fashioned milieu, complete with a very modern happy ending.\"	Mike Nichols	Director Not Available	Director Not Available	Action	PG	5/20/1975	88 minutes	Columbia Pictures	The Fortune	Carole Eastman	Adrien Joyce	Writer Not Available	Writer Not Available	1975");
		ArrayList<String> processTestArrList = test.process(testArrList);
		
		String expected1="2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle";
		String expected2="1975\tThe Fortune\t88 minutes\tkaggle";
		assertEquals(expected1,processTestArrList.get(0));
		assertEquals(expected2,processTestArrList.get(1));
		
		
	}
}