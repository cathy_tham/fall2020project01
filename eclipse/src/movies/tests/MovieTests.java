package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;

/**
 * JUnit Test for Movie.java
 * @author Cathy Tham
 *
 */
class MovieTests {
	
	//Test for toString()
	@Test
	void testToString() {
		Movie movie1=new Movie("2019", "Weathering with You", "111","kaggle");
		String expected = "2019	Weathering with You	111	kaggle";
		assertEquals(expected,movie1.toString());
	}
	
	//Test for getters
	@Test
	void testGetters() {
	Movie movie1=new Movie("2019", "Weathering with You", "111","kaggle");
	Movie movie2=new Movie("2018", "Hotel Transylvania 3: Summer Vacation", "97","kaggle");
	
	assertEquals("2019", movie1.getReleaseYear());
	assertEquals("Weathering with You", movie1.getName());
	assertEquals("111", movie1.getRuntime());
	assertEquals("kaggle", movie1.getSource());
	
	assertEquals("2018", movie2.getReleaseYear());
	assertEquals("Hotel Transylvania 3: Summer Vacation", movie2.getName());
	assertEquals("97", movie2.getRuntime());
	assertEquals("kaggle", movie1.getSource());
	}
	
	//Test for equals() with the time
	@Test
	void testEqualsTime() {
		Movie movie1=new Movie("2019", "Weathering with You", "111","kaggle");
		Movie movie2=new Movie("2019", "Weathering with You", "114","imbd");
		Movie movie3=new Movie("2010", "Weathering with You", "120","kaggle");
		Movie movie4=new Movie("2019", "Weathering with You", "106","kaggle");
		
		assertEquals(movie1,movie2);//time between the 2 is no more than 5 min apart
		assertNotEquals(movie1,movie3);//time between the 2 is more than 5 min apart
		assertEquals(movie1,movie4);//time between the 2 is no more than 5 min apart
		
	}
	
	//Test for equals() with release year
	@Test
	void testEqualsYear() {
		Movie movie1=new Movie("2019", "Weathering with You", "114","kaggle");
		Movie movie2=new Movie("2010", "Weathering with You", "111","imbd");
		Movie movie3=new Movie("2019", "Weathering with You", "111","imbd");
		
		assertNotEquals(movie1,movie2);//releaseYear not equal
		assertEquals(movie1,movie3);//releaseYear equal
	}
	
	//Test for equals() with source and title
	@Test
	void testEqualsTitleSource() {
	Movie movie1=new Movie("2018", "Hotel Transylvania 3: Summer Vacation", "97","kaggle");
	Movie movie2=new Movie("2018", "Hotel Transylvania 3: Summer Vacation", "100","imbd");
	Movie movie3=new Movie("2018", "H�tel Transylvania 3: Summer Vacation", "97","kaggle");
	
	assertNotEquals(movie1,movie3);//title is not the same
	assertEquals(movie1,movie2);//source is different but everything is the same

	}
}
