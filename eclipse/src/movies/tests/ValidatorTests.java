package movies.tests;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import movies.importer.Validator;
import org.junit.jupiter.api.Test;

/**
 * JUnit Test for Validator class
 * @author Guang Zhang
 */

class ValidatorTests {
	// Test if the ArrayList size changes with adding movie with missing field
	@Test
	void testValidatorProcess() {
		Validator testValid = new Validator(" "," ");
		ArrayList<String> testValidArr = new ArrayList<String>();
		testValidArr.add("1894\tMiss Jerry\t45\timdb");
		testValidArr.add("1906\t\t70\timdb");

		ArrayList<String> validatedTestValidArr = testValid.process(testValidArr);
		
		int expectedLength = 1;
		assertEquals(expectedLength, validatedTestValidArr.size());
	}
	
	@Test
	void testValidatorWithStringInRunTime() {
		Validator testValid = new Validator(" ", " ");
		ArrayList<String> testValidArr = new ArrayList <String>();
		testValidArr.add("1894\tMiss Jerry\t45\timdb");
		testValidArr.add("1906\tThe Story of the Kelly Gang\t90m\timdb");
		testValidArr.add("1906\tThe Story of the Kelly Gang\t90\timdb");
		
	
		ArrayList<String> validatedTestValidArr = testValid.process(testValidArr);
		
		String first = "1894\tMiss Jerry\t45\timdb";
		String second = "1906\tThe Story of the Kelly Gang\t90\timdb";
		
		
		assertEquals(first, validatedTestValidArr.get(0));
		assertEquals(second, validatedTestValidArr.get(1));
	}

	@Test
	void testNumberFormatException() {
		try {
			Validator testValid = new Validator(" ", " ");
			ArrayList<String> testValidArr = new ArrayList <String>();
			testValidArr.add("1906\tThe Story of the Kelly Gang\t90m\timdb");		
		
			ArrayList<String> validatedTestValidArr = testValid.process(testValidArr);
			
		} catch(NumberFormatException e) {
				System.out.println("NumberFormatException Expected to show");
			}
	}

}
