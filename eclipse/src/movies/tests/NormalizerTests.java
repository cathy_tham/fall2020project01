package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;

/**
 * JUnit Test for Normalizer
 * @author Cathy Tham
 *
 */
class NormalizerTests {

	/**
	 * Test the process method to see if the normalizer runs correctly
	 * 
	 */
	@Test
	void testProcess() {
		Normalizer test = new Normalizer(" "," ");
		ArrayList<String> testArrList = new ArrayList<String>();
		testArrList.add("2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle");
		testArrList.add("1975\tThe Fortune\t88 minutes\tkaggle");
		ArrayList<String> normalizedTestArrList = test.process(testArrList);
		
		String expected1="2008\tthe mummy: tomb of the dragon emperor\t112\tkaggle";
		String expected2="1975\tthe fortune\t88\tkaggle";
		assertEquals(expected1,normalizedTestArrList.get(0));
		assertEquals(expected2,normalizedTestArrList.get(1));
	}

}
