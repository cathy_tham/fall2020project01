package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Deduper;

/**
 * JUnit Test for Deduper
 * @author Cathy Tham
 *
 */

class DeduperTests {

	/**
	 * Test the process method to see if it does remove duplicate movies
	 * 
	 */
	@Test
	void testRemoveDuplicates() {
		Deduper test = new Deduper(" ", " ");
		ArrayList<String> testArrList = new ArrayList <String>();
		testArrList.add("1975\tThe Fortune\t88\tkaggle");
		testArrList.add("1975\tThe Fortune\t90\timdb");
		ArrayList<String> testMerge = test.process(testArrList);
		
		String expected = "1975\tThe Fortune\t90\timdb;kaggle";
		assertEquals(expected, testMerge.get(0));
	}
	
	@Test
	void testRemoveDuplicatesSameSource() {
		Deduper test = new Deduper(" ", " ");
		ArrayList<String> testArrList = new ArrayList <String>();
		testArrList.add("1975\tThe Fortune\t88\tkaggle");
		testArrList.add("1975\tThe Fortune\t90\tkaggle");
		ArrayList<String> testMerge = test.process(testArrList);
		
		String expected = "1975\tThe Fortune\t90\tkaggle";
		assertEquals(expected, testMerge.get(0));
	}


}
