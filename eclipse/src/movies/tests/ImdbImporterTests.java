package movies.tests;

import movies.importer.ImdbImporter;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/** 
 * JUnit Test for ImdbImporter
 * @author Guang Zhang
 *
 */

class ImdbImporterTests {
	// Test the process method in the ImdbImporter class within package movies.importer
	// Check if it returns to the standardized format
	@Test
	void testProcess() {
		ImdbImporter testImdb = new ImdbImporter(" "," ");
		ArrayList<String> testImdbArr = new ArrayList<String>();
		testImdbArr.add("tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"				7	7");
		ArrayList<String> processedTestImdb = testImdb.process(testImdbArr);
		String expected="1906\tThe Story of the Kelly Gang\t70\timdb";
		assertEquals(expected, processedTestImdb.get(0));
	}

}
