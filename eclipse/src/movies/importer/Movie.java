package movies.importer;

/**
 * Movie class that creates an movie object with a release year, name, runtime, and source.
 *
 */
public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;
	
	/**
	 * Construtor
	 * @author Cathy Tham ; Guang Zhang
	 * @param releaseYear
	 * @param name
	 * @param runtime
	 * @param source
	 */
	public Movie(String releaseYear, String name, String runtime, String source ) {
		this.releaseYear=releaseYear;
		this.name=name;
		this.runtime=runtime;
		this.source=source;
		}
	
	/**
	 * getSource is a get method for the movie's release year
	 * @author Cathy Tham ; Guang Zhang
	 * @return the releaseYear
	 */
	public String getReleaseYear() { 
		return this.releaseYear; 
	}
	
	/**
	 * getSource is a get method for the movie's title
	 * @author Cathy Tham ; Guang Zhang
	 * @return the title of the movie
	 */	
	public String getName() { 
		return this.name; 
	}
	
	/**
	 * getSource is a get method for the movie's runtime
	 * @author Cathy Tham ; Guang Zhang
	 * @return a the runtime of the movie
	 */
	public String getRuntime() { 
		return this.runtime; 
	}
	
	/**
	 * getSource is a get method for the movie's source
	 * @author Cathy Tham ; Guang Zhang
	 * @return a the source of the movie
	 */
	public String getSource() { 
	    return this.source;
	    
	}
	
	@Override
	/**
	 * Override equals method to be able to compare 2 movies 
	 * @return boolean that represent if the 2 movies are the same or not
	 * @author Cathy Tham
	 */
	public String toString() {
		return (this.releaseYear + "\t" + this.name + "\t" +this.runtime+ "\t"+this.source);
	}
		
	@Override
	/**
	 * Override equals to be able to compare 2 movies
	 * If the name and release year are exactly the same and the runtime of the movie is no more than 5 minutes apart.
	 * returns true if it is equal.
	 * @param Object o
	 * @author Cathy Tham
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Movie)) {
			return false;	
		}
		
		Movie movie = (Movie)o;
		//converting string to int
		int oRuntime=Integer.parseInt(movie.runtime);
		int thisRuntime=Integer.parseInt(this.runtime);
			
		if(this.name.equals(movie.name) && this.releaseYear.equals(movie.releaseYear) && (Math.abs(thisRuntime-oRuntime)<=5)) {
			return true;
		}
		else {
			return false ;
		}
	}
}
