package movies.importer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
/**
 * Deduper class extends the Processor class. 
 * Takes in the ArrayList<String> and remove any duplicate movies.
 * It returns a new ArrayList<String> of movies without duplicates.
 * @author Cathy Tham ; Guang Zhang
 *
 */


public class Deduper extends Processor {
	private final int pos_releaseYear=0;
	private final int pos_runTime=2;
	private final int pos_title=1;
	private final int pos_source=3;
	
	public Deduper(String srcDir, String outputDir) {
		super(srcDir, outputDir, false); 
	}
	
	/**
	 * Process is a method that takes an ArrayList<String> to output a file with no duplicated movies
	 * @param input ArrayList<String> that represent imdb and kaggle movies which are validated
	 * @return an ArrayList<String> with no duplicate movie.
	 * @author Cathy Tham
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<Movie> movieNormalized = new ArrayList<Movie>();
		ArrayList<Movie> noDuplicate = new ArrayList<Movie>();
		ArrayList<String> dedup_movies = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			
			String[] txtfile = input.get(i).split("\\t");
			Movie movies=new Movie(txtfile[pos_releaseYear], txtfile[pos_title],txtfile[pos_runTime],txtfile[pos_source]);
			movieNormalized.add(movies);
		}
		
		//Loop through the array 
		for(int i=0; i<movieNormalized.size(); i++) {
			//If there is no duplicate in the new array. See Movie.java for equals override
			if(!(noDuplicate.contains(movieNormalized.get(i)))) {
				//add new movie from the normalized file to a new ArrayList<Movie>
				noDuplicate.add(movieNormalized.get(i));
			}
			//If there is a duplicate movie, we need to merge them before adding to the new ArrayList<Movie>
			else {
				//newString use to merge the two diff source
				String newSource="";
				
				//get the index of the duplicated movie in the noDuplicate ArrayList<Movie>
				int index=noDuplicate.indexOf(movieNormalized.get(i));
				
				//Check if the source is the same, if yes then the source stay the same.
				if(noDuplicate.get(index).getSource().equals(movieNormalized.get(i).getSource())) {
					newSource=movieNormalized.get(i).getSource();
					
				}
				else {
					//merge the source together
					newSource=movieNormalized.get(i).getSource()+ ";" + noDuplicate.get(index).getSource();
				}
				
				Movie movieMerge=new Movie(movieNormalized.get(i).getReleaseYear(), movieNormalized.get(i).getName(),movieNormalized.get(i).getRuntime(),newSource);
				//replace the arr with an new merged arr in the same place of the duplicate movie
				noDuplicate.set(index, movieMerge);
			}
				
		}
		
		//Use to return an ArrayList<String>
		for ( int i= 0; i<noDuplicate.size(); i++) {
			dedup_movies.add(noDuplicate.get(i).toString());
		}
		
		return dedup_movies;

	}
	
}
