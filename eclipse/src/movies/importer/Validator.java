package movies.importer;

import java.util.ArrayList;

/** A class (Validator) extends to the Processor class.
 *  If any field is missing, it is removed from the ArrayList.
 *  Input the ArrayList from the normalized ArrayList.
 *  Returns a new ArrayList<String> without movies having missing fields.
 * @author Guang Zhang
*/

public class Validator extends Processor {
	private final int pos_releaseYear=0;
	private final int pos_title=1;
	private final int pos_runTime=2;
	private final int pos_source=3;
	
		public Validator(String srcDir, String outputDir) {
			super(srcDir, outputDir, false);
		}
		

		public ArrayList<String> process(ArrayList<String> input) {

			//Creating empty array to put all movies that respect the constraints
			ArrayList<String> movieValidated = new ArrayList<String>();
			
			// Loop through input ArrayList and check if fields are missing
 			for(int i = 0;i < input.size();i++) {
 				String[] movieText = input.get(i).split("\\t");
 				if(movieText[pos_releaseYear] != null && !movieText[pos_releaseYear].isEmpty() 
 						&& movieText[pos_title] != null && !movieText[pos_title].isEmpty() 
 						&& movieText[pos_runTime] != null && !movieText[pos_runTime].isEmpty()
 						&& movieText[pos_source] != null && !movieText[pos_source].isEmpty()  ) {
 					/*Try catch to try to verify if it is possible to convert to an int
 					 * If yes, then we add the movie in the ArrayList<String>
 					 */
 					try {
 						Integer.parseInt(movieText[pos_releaseYear]);
 						Integer.parseInt(movieText[pos_runTime]);
 						movieValidated.add(input.get(i));
 					     
 					  }
 					catch(NumberFormatException e) {
 						System.out.println("Wrong number format for ReleaseYear and/or RunTime");
 					}
 					

 				}
 			}
 			return movieValidated;
		}
}
