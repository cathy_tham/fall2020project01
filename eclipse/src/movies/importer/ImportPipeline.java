package movies.importer;

import java.io.IOException;


/**
* ImportPipeline class that utilizes all the processor classes for the movies in a text file. 
* @author Cathy Tham ; Guang Zhang
*
*/

public class ImportPipeline {
	/**
	 * Main() creates Processor[] with the source of KaggleImporter, ImdbImporter, Normalizer, Validator, Deduper files and call processAll() on the array.
	 */
	public static void main(String[] args){
		String srcKaggle="C:\\cathy\\courses\\java310\\fall2020project01\\files\\kaggleTestFiles";
		String srcImdb="C:\\cathy\\courses\\java310\\fall2020project01\\files\\imdbTestFiles";
		String srcImported="C:\\cathy\\courses\\java310\\fall2020project01\\files\\standardizedTestFiles";
		String srcNormalized="C:\\cathy\\courses\\java310\\fall2020project01\\files\\normalizedTestFiles";
		String srcValidated="C:\\cathy\\courses\\java310\\fall2020project01\\files\\validatedTestFiles";
		String srcFinalDeduplicated="C:\\cathy\\courses\\java310\\fall2020project01\\files\\deduplicatedTestFiles";
		
//		String srcKaggle="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//kaggleTestFiles";
//		String srcImdb="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//imdbTestFiles";
//		String srcImported="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//standardizedTestFiles";
//		String srcNormalized="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//normalizedTestFiles";
//		String srcValidated="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//validatedTestFiles";
//		String srcFinalDeduplicated="//Users//guang//Desktop//ProgrammingIII//java310//fall2020project01//files//deduplicatedTestFiles";

		KaggleImporter kaggleImp = new KaggleImporter(srcKaggle, srcImported);
		ImdbImporter imdbImp = new ImdbImporter(srcImdb, srcImported);
		Normalizer normalized = new Normalizer(srcImported, srcNormalized);
		Validator validated = new Validator(srcNormalized, srcValidated);
		Deduper finalDedup = new Deduper(srcValidated, srcFinalDeduplicated);
		
		Processor[] processes= new Processor[5];
		processes[0]=kaggleImp;
		processes[1]=imdbImp;
		processes[2]=normalized;
		processes[3]=validated;
		processes[4]=finalDedup;
		
		//To handle IOException= try, catch 
		try {
		processAll(processes);
		}
		catch(IOException e) {
			System.out.println("Error: IOException");
		}
		catch (NullPointerException e) {
			System.out.println("Error: no such file found");
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("Error: IndexOutOfBoundsException");
		}
		catch(NumberFormatException e) {
				System.out.println("Wrong number format for ReleaseYear and/or RunTime");
		}
	}
	
	/**
	 * processAll() calls execute() on each of the methods inside each element of the array
	 * @param processes Processor[] that represent each file in each directory
	 * @throws IOException
	 */
	public static void processAll(Processor[] processes) throws IOException {
		for (Processor p: processes) {
			p.execute();
		}
	}


}