package movies.importer;

import java.util.ArrayList;

/**
 * Normalizer class extends the Processor class. 
 * It returns the ArrayList<String> normalized with the title in lower case and the first word of the runtime.
 * @author Cathy Tham
 *
 */
public class Normalizer extends Processor{
	private final int pos_releaseYear=0;
	private final int pos_runTime=2;
	private final int pos_title=1;
	private final int pos_source=3;
	
	/**
	 * Parameterized constructor for Normalizer to extend Processor class
	 * @param sourceDir
	 * @param outputDir
	 */
	public Normalizer(String srcDir, String outputDir) {
		super(srcDir, outputDir, false); 
		
	}
	
	/**
	 * Process is a method that keeps the release year, the source and only the first word of the runtime and changing the titles to lower case
	 * @param input ArrayList<String> that represent a standardized ArrayList<String> 
	 * @return an ArrayList<String> that represent a normalized ArrayList<String>.
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> movieTextProcessed = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			String[] txtProcessed = input.get(i).split("\\t");
			
			//Splitting the runTime every time there is a space
			String[] runTime=txtProcessed[pos_runTime].split(" ");
			//Taking only the first word of the runTime 
			String firstWord=runTime[0];

			Movie movies=new Movie(txtProcessed[pos_releaseYear],txtProcessed[pos_title].toLowerCase(), firstWord,txtProcessed[pos_source]);
			movieTextProcessed.add(movies.toString());
		}
		
    return movieTextProcessed;     
	}

	

}
