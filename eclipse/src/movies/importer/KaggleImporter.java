package movies.importer;

import java.util.ArrayList;

/**
 * KaggleImporter class extends the Processor class. 
 * It returns a new ArrayList<String> that are standardized to match what a Movie should look like.
 * @author Cathy Tham
 *
 */
public class KaggleImporter extends Processor {
	private final int pos_releaseYear=20;
	private final int pos_runTime=13;
	private final int pos_title=15;
	private final String source="kaggle";
	private final int total_column=21; //compare with length
	
	/**
	 * Parameterized constructor for KaggleImporter to extend Processor class
	 * @param sourceDir
	 * @param outputDir
	 */
	public KaggleImporter(String srcDir, String outputDir) {
		super(srcDir, outputDir, true); 
		
	}

	/**
	 * Process is a method that takes an ArrayList<String> that came from the raw txt file and create a new file with title, releaseYear, runTime and source
	 * @param input ArrayList<String> that represent raw txt file of kaggle movies 
	 * @return an ArrayList<String> that represent a standardized ArrayList<String>.
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> movieText = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			String[] txtfile = input.get(i).split("\\t");

			/*If the total column in the line is equal to the txtfile arr length,
			 *we add it to the movieText arr by using the toString().
			 *Else, the code skips this line */
			if(txtfile.length==total_column) {
			Movie movies=new Movie(txtfile[pos_releaseYear], txtfile[pos_title],txtfile[pos_runTime],source);
			movieText.add(movies.toString());
			}
		}
		
    return movieText;     
	}


}

