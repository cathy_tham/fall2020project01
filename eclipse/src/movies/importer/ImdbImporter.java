package movies.importer;

import java.util.*;


/** A class (ImdbImporter) extends to the Processor class.
 * Returns a new ArrayList<String> for standardized movie format.
 * @author Guang Zhang 
*/

public class ImdbImporter extends Processor {
	private final String source="imdb";
	private final int total_fields=22;
	private final int pos_releaseYear=3;
	private final int pos_runTime=6;
	private final int pos_title=1;

	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	/**
	 * Process method takes in the raw text files as ArrayList<String>
	 * Returns new ArrayList<String> after processing the raw text files for further processing
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> movieList = new ArrayList<String>();
		
		for(int i=0; i<input.size(); i++) {
			String[] movieText = input.get(i).split("\\t");
			/**
			 * Check if the number of fields for the movie matches the total fields
			 * if matches, then create Movie and add to movieList 
			 */
			if (movieText.length==total_fields) {
				Movie newMovie=new Movie(movieText[pos_releaseYear], movieText[pos_title],movieText[pos_runTime],source);

//				Movie newMovie = new Movie(movieText[3],movieText[1],movieText[6],source);
				movieList.add(newMovie.toString());
			}
		}
		return movieList;
	}

}
